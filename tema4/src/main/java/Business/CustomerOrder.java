package Business;

public class CustomerOrder {

	private int id;
	private int tableNo;
	private int price;

	public CustomerOrder() {
		// TODO Auto-generated constructor stub
	}

	public CustomerOrder(int id, int tableNo, int price) {
		// TODO Auto-generated constructor stub
		this.id = id;
		this.tableNo = tableNo;
		this.price = price;
	}

	public CustomerOrder(int tableNo, String productName, int price) {
		// TODO Auto-generated constructor stub
		this.tableNo = tableNo;
		this.price = price;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getTableNo() {
		return tableNo;
	}

	public void setTableNo(int tableNo) {
		this.tableNo = tableNo;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	@Override
	public int hashCode() {
		Integer inte = this.tableNo;
		int res = 17;
		res = 19 * res + this.id;
		res = 19 * res + inte.hashCode();

		return res;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null || obj.getClass() != this.getClass())
			return false;
		CustomerOrder clientOrder = (CustomerOrder) obj;
		return (clientOrder.id == this.id && clientOrder.tableNo == this.tableNo);
	}
}
