package Business;

import java.util.List;

public interface RestaurantProcessing {

	/**
	 * 
	 * @pre menuItem != null
	 * @post @nochange
	 */
	public void createNewMenuItem(MenuItem menuItem);

	/**
	 * 
	 * @pre name != null
	 * @post @nochange
	 */
	public void deleteMenuItem(MenuItem menuItem);

	/**
	 * 
	 * @pre menuItem != null && newPrice != 0
	 * @post menuItem.name.equals)(edited menuItem.name)
	 */
	public void editMenuItem(MenuItem menuItem, int newPrice);

	/**
	 * 
	 * @pre order.isEmpty() == false && Integer.parseInt(tableno) > 0
	 * @post @nochange
	 */
	public void createNewOrder(List<OrderDetails> order, String tableno);

	public void generateBill();
}
