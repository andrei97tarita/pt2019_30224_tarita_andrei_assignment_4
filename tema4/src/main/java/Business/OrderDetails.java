package Business;

public class OrderDetails {

	private int id;
	private int idProduct;
	private int idOrder;
	private int quantity;
	private MenuItem menuItem;

	public OrderDetails() {
		// TODO Auto-generated constructor stub
	}

	public OrderDetails( int quantity, MenuItem menuItem) {
		// TODO Auto-generated constructor stub
	
		this.quantity = quantity;
		this.setMenuItem(menuItem);
	}

	public OrderDetails(int idProduct, int idOrder, int quantity) {
		// TODO Auto-generated constructor stub
		this.idProduct = idProduct;
		this.idOrder = idOrder;
		this.quantity = quantity;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdProduct() {
		return idProduct;
	}

	public void setIdProduct(int idProduct) {
		this.idProduct = idProduct;
	}

	public int getIdOrder() {
		return idOrder;
	}

	public void setIdOrder(int idOrder) {
		this.idOrder = idOrder;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public MenuItem getMenuItem() {
		return menuItem;
	}

	public void setMenuItem(MenuItem menuItem) {
		this.menuItem = menuItem;
	}

}
