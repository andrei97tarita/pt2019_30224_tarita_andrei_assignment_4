package Business;

import java.io.Serializable;

public class MenuItem implements Serializable {

	//private static final long serialVersionUID = 1L;
	private int id;
	private int itemtype;
	private String itemname;
	private int itemprice;

	public MenuItem(String name, int type, int price) {
		// TODO Auto-generated constructor stub
		this.setItemtype(type);
		this.setItemname(name);
		this.setItemprice(price);
	}

	public MenuItem(String name) {
		// TODO Auto-generated constructor stub
		this.setItemname(name);
	}

	public MenuItem(int id, String name, int type, int price) {
		// TODO Auto-generated constructor stub
		this.id = id;
		this.setItemtype(type);
		this.setItemname(name);
		this.setItemprice(price);
	}

	public MenuItem() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void computePrice() {
	}

	public int getItemtype() {
		return itemtype;
	}

	public void setItemtype(int itemtype) {
		this.itemtype = itemtype;
	}

	public String getItemname() {
		return itemname;
	}

	public void setItemname(String itemname) {
		this.itemname = itemname;
	}

	public int getItemprice() {
		return itemprice;
	}

	public void setItemprice(int itemprice) {
		this.itemprice = itemprice;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.id + " " + this.itemname + " " + this.itemtype + " " + this.itemprice;
	}
}
