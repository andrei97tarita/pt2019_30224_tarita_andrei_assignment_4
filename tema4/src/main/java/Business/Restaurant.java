package Business;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.*;

import javax.swing.JOptionPane;

import Data.AbstractAccess;
import Data.DataMenuItem;
import Data.DataOrder;
import Data.RestaurantSerializator;

public class Restaurant implements RestaurantProcessing {

	private static HashMap<CustomerOrder, List<OrderDetails>> restaurantData = new HashMap<>();
	private Set<MenuItem> restaurantMenu = new HashSet<>();
	Restaurant restaurant;

	public void createMenu() throws FileNotFoundException {
		new DataMenuItem();
		File file = new File("menu.txt");
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(file);

		while (scanner.hasNext()) {
			MenuItem menu = new MenuItem(scanner.nextInt(), scanner.next(), scanner.nextInt(), scanner.nextInt());
			restaurantMenu.add(menu);
			new Restaurant().createNewMenuItem(menu);
		}
	}

	public void addData(CustomerOrder order, List<OrderDetails> menuItems) {
		restaurantData.put(order, menuItems);
	}

	@Override
	public void createNewMenuItem(MenuItem menuItem) {
		// TODO Auto-generated method stub
		assert (menuItem != null);
		new DataMenuItem().addMenuItem(menuItem);
	}

	@Override
	public void deleteMenuItem(MenuItem menuItem) {
		// TODO Auto-generated method stub
		assert (menuItem != null);
		new DataMenuItem().removeMenuItem(menuItem);
	}

	@Override
	public void editMenuItem(MenuItem menuItem, int newPrice) {
		// TODO Auto-generated method stub
		assert (menuItem != null);
		new DataMenuItem().updateMenuItem(menuItem, newPrice);
	}

	@Override
	public void createNewOrder(List<OrderDetails> orderDetails, String tableNo) {
		restaurant = new Restaurant();
		// TODO Auto-generated method stub
		assert (orderDetails.size() > 0);
		CustomerOrder customerOrder = new CustomerOrder(Integer.parseInt(tableNo), "", 0);
		new DataOrder().addOrder(customerOrder, orderDetails);
		restaurantData.put(customerOrder, orderDetails);

	}

	public static void main(String[] args) throws FileNotFoundException {
		RestaurantSerializator ser = new RestaurantSerializator();
		System.out.println(restaurantData.isEmpty());
		System.out.println(restaurantData.size());

		List<MenuItem> menuItems = new ArrayList<MenuItem>();

		menuItems.add(new MenuItem(0, "cas", 1, 22));
		menuItems.add(new MenuItem(0, "paine", 1, 24));
		System.out.println("\nda" + menuItems);

		ser.serialization(menuItems);

//		new Restaurant().createMenu();
	}

	public void generateBill() {
		// TODO Auto-generated method stub
		Writer writer = null;
		int totalPrice = 0;
		String orderno = JOptionPane.showInputDialog("Order: ");
		try {
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("filename.txt"), "utf-8"));
			writer.write("OrderID: " + Integer.parseInt(orderno) + "\n");

			List<OrderDetails> dataOrder = new DataOrder().generateOrders(Integer.parseInt(orderno));

			for (OrderDetails customerOrder : dataOrder) {
				MenuItem item = new AbstractAccess<MenuItem>(MenuItem.class)
						.getColumnByID(customerOrder.getIdProduct());
//				MenuItem item = list.get(customerOrder.getIdProduct());
				writer.write("\t" + item.getItemname() + ": " + item.getItemprice() + " x "
						+ customerOrder.getQuantity() + "\n");
				totalPrice += item.getItemprice() * customerOrder.getQuantity();
				writer.write("\n");
			}
			writer.write("---------- Total: " + totalPrice);
		} catch (IOException ex) {
			// Report
		} finally {
			try {
				writer.close();
			} catch (Exception ex) {
				/* ignore */}
		}
	}
}
