package Data;

public interface Observer {

	public void update(String obj);
}
