package Data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import Business.MenuItem;

public class DataMenuItem {

	private String insertProductInQuery = "insert into menuitem (itemname, itemtype, itemprice) values (?, ?, ?);";
	private String removeProductInQuery = "delete from menuitem where itemname = ?;";
	private String editProductInQuery = "update menuitem set itemprice = ? where itemname = ?;";

	public DBConnection database = new DBConnection();

	public DataMenuItem() {
		// TODO Auto-generated constructor stub
		this.database = new DBConnection();
	}

	public void addMenuItem(MenuItem menuItem) {

		Connection connection = DBConnection.getConnection();

		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(insertProductInQuery);
			statement.setString(1, menuItem.getItemname());
			statement.setInt(2, menuItem.getItemtype());
			statement.setInt(3, menuItem.getItemprice());
			statement.execute();
			System.out.println("Succesfuly added menuItem!");
		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			statement.close();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void removeMenuItem(MenuItem menuItem) {

		RestaurantSerializator ser = new RestaurantSerializator();

		List<MenuItem> menuItems = ser.deserialization();

		for (MenuItem menuItem2 : menuItems) {
			if (menuItem2.getItemname().equals(menuItem.getItemname())) {
				menuItems.remove(menuItem2);
				break;
			}
		}
		ser.serialization(menuItems);

	}

	public void updateMenuItem(MenuItem menuItem, int newPrice) {

		RestaurantSerializator ser = new RestaurantSerializator();

		List<MenuItem> menuItems = ser.deserialization();

		for (MenuItem menuItem2 : menuItems) {
			if (menuItem2.getItemname().equals(menuItem.getItemname())) {
				menuItem2.setItemprice(newPrice);
				break;
			}
		}
		ser.serialization(menuItems);
	}

	public int getNumberOfItems() {
		Connection conn = DBConnection.getConnection();
		ResultSet resultSet = null;
		PreparedStatement findStatement = null;
		int nr = 0;

		try {
			findStatement = (PreparedStatement) conn.prepareStatement("select * from menuitem;");
			findStatement.execute();
			resultSet = findStatement.getResultSet();
			while (resultSet.next()) {
				nr++;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("EROARE");
			e.printStackTrace();
		}

		return nr;
	}
}
