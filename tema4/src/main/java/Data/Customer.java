package Data;

public class Customer {

	private int id;
	private int tableNo;
	private int price;

	public Customer(int id, int table, int price) {
		// TODO Auto-generated constructor stub
		this.id = id;
		this.tableNo = table;
		this.price = price;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getTable() {
		return tableNo;
	}

	public void setTable(int table) {
		this.tableNo = table;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
}
