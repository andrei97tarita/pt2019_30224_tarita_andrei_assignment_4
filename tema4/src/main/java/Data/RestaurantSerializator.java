package Data;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import Business.MenuItem;

public class RestaurantSerializator {

	public void serialization(List<MenuItem> menuItems) {

		try {
			FileOutputStream fileOut = new FileOutputStream("OutSerial.ser");
			ObjectOutputStream out = new ObjectOutputStream(fileOut); 
			out.writeObject(menuItems);
			out.close();
			fileOut.close();
			System.out.printf("Serialized data is saved in " + fileOut.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public List<MenuItem> deserialization() {
		List<MenuItem> items = new ArrayList<MenuItem>();
		try {
			FileInputStream fileIn = new FileInputStream("OutSerial.ser");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			items = (List<MenuItem>) in.readObject();
			in.close();
			fileIn.close();
		} catch (IOException e) {
			e.printStackTrace();
			return items;
		} catch (ClassNotFoundException c) {
			c.printStackTrace();
			return items;
		}
		return items;
	}
}