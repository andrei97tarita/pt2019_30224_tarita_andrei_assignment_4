package Data;

import java.util.ArrayList;
import java.util.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import Business.CustomerOrder;
import Business.MenuItem;
import Business.OrderDetails;

public class DataOrder {

	private String insertProductInQuery = "insert into customerOrder (tableNo, price) values (?, ?);";
	private String getOrderId = "select count(*) from customerOrder;";
	private String getOrders = "select * from orderdetails where idorder = ?;";

	public DBConnection database = new DBConnection();

	public DataOrder() {
		// TODO Auto-generated constructor stub
		this.database = new DBConnection();
	}

	public void addOrder(CustomerOrder customerOrder, List<OrderDetails> orderDetails) {

		Connection connection = DBConnection.getConnection();

		int id = 0;
		int totalPrice = 0;

		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(insertProductInQuery);
			statement.setInt(1, customerOrder.getTableNo());
			statement.setInt(2, customerOrder.getPrice());
			statement.execute();

			statement = connection.prepareStatement(getOrderId);
			statement.execute();
			ResultSet resultSet = statement.getResultSet();
			while (resultSet.next()) {
				id = resultSet.getInt(1);
			}
			for (OrderDetails details : orderDetails) {
				statement = connection
						.prepareStatement("insert into orderDetails(idProduct, idOrder, quantity) values (?,?,?);");
				statement.setInt(1, details.getIdProduct());
				statement.setInt(2, id);
				statement.setInt(3, details.getQuantity());
				statement.execute();
				totalPrice += new AbstractAccess<MenuItem>(MenuItem.class).getColumnByID(details.getIdProduct())
						.getItemprice() * details.getQuantity();

			}

			statement = connection.prepareStatement("UPDATE customerOrder SET price = ? WHERE id = ?;");
			statement.setInt(1, totalPrice);
			statement.setInt(2, id);
			statement.execute();
			System.out.println("Succesfuly added order!");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			statement.close();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public int getNumberOfItems() {
		Connection conn = DBConnection.getConnection();
		ResultSet resultSet = null;
		PreparedStatement findStatement = null;
		int nr = 0;

		try {
			findStatement = (PreparedStatement) conn.prepareStatement("select * from customerOrder;");
			findStatement.execute();
			resultSet = findStatement.getResultSet();
			while (resultSet.next()) {
				nr++;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("EROARE");
			e.printStackTrace();
		}

		return nr;
	}

	public List<OrderDetails> generateOrders(int id) {
		Connection conn = DBConnection.getConnection();
		ResultSet resultSet = null;
		PreparedStatement findStatement = null;
		List<OrderDetails> details = new ArrayList<OrderDetails>();

		try {
			findStatement = (PreparedStatement) conn.prepareStatement(getOrders);
			findStatement.setInt(1, id);
			findStatement.execute();
			resultSet = findStatement.getResultSet();
			while (resultSet.next()) {
				details.add(new OrderDetails(resultSet.getInt(2), resultSet.getInt(3), resultSet.getInt(4)));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("EROARE");
			e.printStackTrace();
		}
		return details;
	}
}
