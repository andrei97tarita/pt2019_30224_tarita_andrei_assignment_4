package Data;

import java.util.List;

import Business.OrderDetails;

public interface Subject {

	public void registerObserver(Observer observer);

	public void removeObserver(Observer oberver);

	public void notifyObservers(List<OrderDetails> order);
}
