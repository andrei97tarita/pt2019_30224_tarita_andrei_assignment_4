package Presentation;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import Business.MenuItem;
import Business.Restaurant;
import Data.RestaurantSerializator;

public class AdministratorGraphicalUserInterface {

	private JFrame adminFrame = new JFrame("Administrator");

	private JFrame addMenuItemFrame = new JFrame("Add MenuItem");
	private JFrame deleteMenuItemFrame = new JFrame("Delete MenuItem");
	private JFrame editMenuItemFrame = new JFrame("Edit MenuItem");
	private JFrame viewMenuItemFrame = new JFrame("View MenuItem");

	private JTextField nameTextField = new JTextField();
	private JTextField typeTextField = new JTextField();
	private JTextField priceTextField = new JTextField();

	private JButton addMenuItemBtn = new JButton("add");
	private JButton removeMenuItemBtn = new JButton("delete");
	private JButton changeMenuItemBtn = new JButton("edit");
	@SuppressWarnings("unused")
	private FileOutputStream fileOut = null;

	public void initAdminUI() {
		try {
			fileOut = new FileOutputStream("OutSerial.ser", true);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		adminFrame.setBounds(200, 200, 615, 300);
		adminFrame.setVisible(true);
		adminFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		adminFrame.setLayout(null);

		JButton createMenuItemBtn = new JButton("add MenuItem");
		createMenuItemBtn.setBounds(20, 80, 130, 50);
		createMenuItemBtn.setVisible(true);
		JButton deleteMenuItemBtn = new JButton("delete MenuItem");
		deleteMenuItemBtn.setBounds(160, 80, 130, 50);
		deleteMenuItemBtn.setVisible(true);
		JButton editMenuItemBtn = new JButton("edit Menu Item");
		editMenuItemBtn.setBounds(300, 80, 130, 50);
		editMenuItemBtn.setVisible(true);
		JButton viewMenuItemBtn = new JButton("view Menu Item");
		viewMenuItemBtn.setBounds(440, 80, 130, 50);
		viewMenuItemBtn.setVisible(true);

		adminFrame.add(createMenuItemBtn);
		adminFrame.add(deleteMenuItemBtn);
		adminFrame.add(editMenuItemBtn);
		adminFrame.add(viewMenuItemBtn);

		createMenuItemBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				addMenuItemFrame.setBounds(200, 200, 615, 300);
				addMenuItemFrame.setVisible(true);
				addMenuItemFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				addMenuItemFrame.setLayout(null);

				addMenuItemBtn.setBounds(80, 140, 130, 50);
				addMenuItemBtn.setVisible(true);

				JLabel nameLabel = new JLabel("name: ");
				nameLabel.setBounds(10, 10, 80, 20);
				nameLabel.setVisible(true);
				nameTextField.setBounds(80, 10, 200, 20);
				nameTextField.setVisible(true);
				JLabel typeLabel = new JLabel("type: ");

				typeLabel.setBounds(10, 50, 80, 20);
				typeLabel.setVisible(true);
				typeTextField.setBounds(80, 50, 200, 20);
				typeTextField.setVisible(true);
				JLabel priceLabel = new JLabel("price: ");

				priceLabel.setBounds(10, 90, 80, 20);
				priceLabel.setVisible(true);
				priceTextField.setBounds(80, 90, 200, 20);
				priceTextField.setVisible(true);

				addMenuItemFrame.add(nameLabel);
				addMenuItemFrame.add(nameTextField);
				addMenuItemFrame.add(typeLabel);
				addMenuItemFrame.add(typeTextField);
				addMenuItemFrame.add(priceLabel);
				addMenuItemFrame.add(priceTextField);
				addMenuItemFrame.add(addMenuItemBtn);

				addMenuItemBtn.addActionListener(new ActionListener() {
					List<MenuItem> list = new ArrayList<MenuItem>();

					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						MenuItem menuItem = new MenuItem(nameTextField.getText(),
								Integer.parseInt(typeTextField.getText()), Integer.parseInt(priceTextField.getText()));
						new Restaurant().createNewMenuItem(new MenuItem(nameTextField.getText(),
								Integer.parseInt(typeTextField.getText()), Integer.parseInt(priceTextField.getText())));
						list = new RestaurantSerializator().deserialization();
						list.add(menuItem);
						new RestaurantSerializator().serialization(list);

						addMenuItemFrame.dispose();
					}
				});
			}
		});

		deleteMenuItemBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				deleteMenuItemFrame.setBounds(200, 200, 615, 300);
				deleteMenuItemFrame.setVisible(true);
				deleteMenuItemFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				deleteMenuItemFrame.setLayout(null);

				removeMenuItemBtn.setBounds(80, 140, 130, 50);
				removeMenuItemBtn.setVisible(true);

				JLabel nameLabel = new JLabel("name: ");
				nameLabel.setBounds(10, 10, 80, 20);
				nameLabel.setVisible(true);
				nameTextField.setBounds(80, 10, 200, 20);
				nameTextField.setVisible(true);

				deleteMenuItemFrame.add(nameLabel);
				deleteMenuItemFrame.add(nameTextField);
				deleteMenuItemFrame.add(removeMenuItemBtn);

				removeMenuItemBtn.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						new Restaurant().deleteMenuItem(new MenuItem(nameTextField.getText()));
					}
				});
			}
		});

		editMenuItemBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				editMenuItemFrame.setBounds(200, 200, 615, 300);
				editMenuItemFrame.setVisible(true);
				editMenuItemFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				editMenuItemFrame.setLayout(null);

				changeMenuItemBtn.setBounds(80, 140, 130, 50);
				changeMenuItemBtn.setVisible(true);

				JLabel nameLabel = new JLabel("name: ");
				nameLabel.setBounds(10, 10, 80, 20);
				nameLabel.setVisible(true);
				nameTextField.setBounds(80, 10, 200, 20);
				nameTextField.setVisible(true);

				JLabel priceLabel = new JLabel("new Price: ");
				priceLabel.setBounds(10, 50, 80, 20);
				priceLabel.setVisible(true);
				priceTextField.setBounds(80, 50, 200, 20);
				priceTextField.setVisible(true);

				editMenuItemFrame.add(nameLabel);
				editMenuItemFrame.add(nameTextField);
				editMenuItemFrame.add(priceLabel);
				editMenuItemFrame.add(priceTextField);
				editMenuItemFrame.add(changeMenuItemBtn);

				changeMenuItemBtn.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						new Restaurant().editMenuItem(new MenuItem(nameTextField.getText()),
								Integer.parseInt(priceTextField.getText()));
					}
				});
			}
		});

		viewMenuItemBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				List<MenuItem> list = new RestaurantSerializator().deserialization();
				// TODO Auto-generated method stub
				viewMenuItemFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				viewMenuItemFrame.setBounds(200, 200, 700, 450);
				viewMenuItemFrame.setResizable(true);
				viewMenuItemFrame.setVisible(true);
				viewMenuItemFrame.setLayout(null);

				String[] arg = new String[MenuItem.class.getDeclaredFields().length];

				int idx = 0;
				for (Field field : MenuItem.class.getDeclaredFields()) {
					arg[idx++] = field.getName();
				}

				JTable itemMenuTable = new JTable();
				@SuppressWarnings("serial")
				DefaultTableModel model = new DefaultTableModel(arg, list.size()) {

					@Override
					public boolean isCellEditable(int row, int column) {
						// TODO Auto-generated method stub
						return false;
					}

					public Class<?> getColumnClass(int col) {
						return String.class;
					}
				};

				int index = 0;

				for (int j = 0; j < list.size(); j++) {

					if (list.get(j) != null) {
						model.setValueAt(list.get(j).getId(), index, 0);
						model.setValueAt(list.get(j).getItemtype(), index, 1);
						model.setValueAt(list.get(j).getItemname(), index, 2);
						model.setValueAt(list.get(j).getItemprice(), index, 3);
						index++;
					}
				}

				itemMenuTable.setModel(model);
				itemMenuTable.setBounds(30, 50, 500, 200);
				itemMenuTable.setPreferredScrollableViewportSize(new Dimension(600, 200));

				DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
				cellRenderer.setHorizontalAlignment(JLabel.CENTER);
				itemMenuTable.setCellSelectionEnabled(false);
				itemMenuTable.setRowSelectionAllowed(true);
				itemMenuTable.setDefaultRenderer(String.class, cellRenderer);
				itemMenuTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
				itemMenuTable.getTableHeader().setReorderingAllowed(false);
				viewMenuItemFrame.add(itemMenuTable);

				JScrollPane pane = new JScrollPane(itemMenuTable);
				pane.setBounds(30, 50, 610, 200);
				pane.setVisible(true);
				viewMenuItemFrame.add(pane);

			}
		});
	}

}
