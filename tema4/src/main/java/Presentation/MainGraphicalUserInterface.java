package Presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

public class MainGraphicalUserInterface {

	private JFrame mainFrame = new JFrame("Restaurant");

	public void initMainUI() {

		mainFrame.setBounds(200, 200, 615, 300);
		mainFrame.setVisible(true);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.setLayout(null);

		JButton adminBtn = new JButton("Admin");
		adminBtn.setBounds(120, 80, 130, 50);
		adminBtn.setVisible(true);
		JButton waiterBtn = new JButton("Waiter");
		waiterBtn.setBounds(320, 80, 130, 50);
		waiterBtn.setVisible(true);

		mainFrame.add(adminBtn);
		mainFrame.add(waiterBtn);

		adminBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				new AdministratorGraphicalUserInterface().initAdminUI();
			}
		});

		waiterBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				new WaiterGraphicalUserInterface().initWaiterUI();
			}
		});
	}

	public static void main(String[] args) {
		new MainGraphicalUserInterface().initMainUI();
	}

}
