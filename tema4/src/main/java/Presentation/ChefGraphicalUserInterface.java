package Presentation;

import Data.AbstractAccess;
import Data.Observer;

import java.util.List;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;

import Business.MenuItem;
import Business.OrderDetails;

public class ChefGraphicalUserInterface implements Observer {

	private JTextArea orderText = new JTextArea();

	public void update(String string) {
		// TODO Auto-generated method stub
		this.orderText.setText(string);
	}

	public String getState() {
		return orderText.getText();
	}

	ChefGraphicalUserInterface() {
	}
	
	void editFrame(List<OrderDetails> order, int tableNo) {
		orderText.setText("");
		orderText.setText("Table number: " + tableNo + "\n");
		for (OrderDetails customerOrder : order) {
			MenuItem item = customerOrder.getMenuItem();
			orderText.setText(orderText.getText() + item.getItemname() + " x " + customerOrder.getQuantity() + "\n");
		}
	}

	void initialize() {
		JFrame frame = new JFrame();
		frame.setBounds(900, 200, 430, 300);
		frame.setVisible(true);
		frame.setLayout(null);
		frame.setTitle("Chef");

		orderText.setBounds(45, 20, 300, 200);
		orderText.setEditable(false);
		orderText.setAlignmentX(JLabel.CENTER);
		orderText.setAlignmentY(JLabel.CENTER);

		
		frame.add(orderText);
	}
}
