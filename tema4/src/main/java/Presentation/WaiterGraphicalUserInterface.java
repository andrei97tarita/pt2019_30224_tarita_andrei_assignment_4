package Presentation;

import java.awt.Dimension;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import Business.CustomerOrder;
import Business.MenuItem;
import Business.OrderDetails;
import Business.Restaurant;
import Data.AbstractAccess;
import Data.DataMenuItem;
import Data.Observer;
import Data.RestaurantSerializator;
import Data.Subject;

public class WaiterGraphicalUserInterface implements Subject {

	private JFrame waiterFrame = new JFrame("Waiter");
	private JFrame viewProductsFrame = new JFrame("MenuItem");
	private JFrame viewOrdersFrame = new JFrame("Orders");
	private JButton placeOrderBtn = new JButton("Place");

	private JTable itemMenuTable = new JTable();

	protected List<Observer> observers = new ArrayList<Observer>();
	protected String string;
	private ChefGraphicalUserInterface chefWindow;
	private int tableNo;
	private List<MenuItem> list = new RestaurantSerializator().deserialization();

	public void initWaiterUI() {

		waiterFrame.setBounds(200, 200, 500, 300);
		waiterFrame.setVisible(true);
		waiterFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		waiterFrame.setLayout(null);

		JButton createNewOrderBtn = new JButton("add Order");
		createNewOrderBtn.setBounds(20, 80, 130, 50);
		createNewOrderBtn.setVisible(true);
		JButton viewOrdersBtn = new JButton("view Orders");
		viewOrdersBtn.setBounds(160, 80, 130, 50);
		viewOrdersBtn.setVisible(true);
		JButton generateBillBtn = new JButton("generate Bill");
		generateBillBtn.setBounds(300, 80, 130, 50);
		generateBillBtn.setVisible(true);

		waiterFrame.add(createNewOrderBtn);
		waiterFrame.add(viewOrdersBtn);
		waiterFrame.add(generateBillBtn);

		createNewOrderBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				chefWindow = new ChefGraphicalUserInterface();
				chefWindow.initialize();
				// TODO Auto-generated method stub
				viewProductsFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				viewProductsFrame.setBounds(200, 200, 700, 450);
				viewProductsFrame.setResizable(true);
				viewProductsFrame.setVisible(true);
				viewProductsFrame.setLayout(null);

				String[] arg = new String[MenuItem.class.getDeclaredFields().length + 1];

				int idx = 0;
				for (Field field : MenuItem.class.getDeclaredFields()) {
					arg[idx++] = field.getName();
				}
				arg[idx] = "Cantitate";

				int rows = new DataMenuItem().getNumberOfItems();

				@SuppressWarnings("serial")
				DefaultTableModel model = new DefaultTableModel(arg, list.size()) {

					@Override
					public boolean isCellEditable(int row, int column) {
						// TODO Auto-generated method stub
						if (column == 4) {
							return true;
						}
						return false;
					}

					public Class<?> getColumnClass(int col) {
						return String.class;
					}
				};

				int index = 0;
				for (int j = 0; j < list.size(); j++) {
					if (list.get(j) != null) {
						model.setValueAt(list.get(j).getId(), index, 0);
						model.setValueAt(list.get(j).getItemtype(), index, 1);
						model.setValueAt(list.get(j).getItemname(), index, 2);
						model.setValueAt(list.get(j).getItemprice(), index, 3);
						model.setValueAt(0, index, 4);
						index++;
					}
				}

				itemMenuTable.setModel(model);
				itemMenuTable.setBounds(30, 50, 500, 200);
				itemMenuTable.setPreferredScrollableViewportSize(new Dimension(600, 200));

				DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
				cellRenderer.setHorizontalAlignment(JLabel.CENTER);
				itemMenuTable.setCellSelectionEnabled(false);
				itemMenuTable.setRowSelectionAllowed(true);
				itemMenuTable.setDefaultRenderer(String.class, cellRenderer);
				itemMenuTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
				itemMenuTable.getTableHeader().setReorderingAllowed(false);
				viewProductsFrame.add(itemMenuTable);

				JScrollPane pane = new JScrollPane(itemMenuTable);
				pane.setBounds(30, 50, 610, 200);
				pane.setVisible(true);
				viewProductsFrame.add(pane);

				placeOrderBtn.setBounds(30, 260, 80, 30);
				placeOrderBtn.setVisible(true);
				viewProductsFrame.add(placeOrderBtn);
			}
		});

		placeOrderBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

				String tableno = JOptionPane.showInputDialog("Table: ");
				tableNo = Integer.parseInt(tableno);

				List<OrderDetails> order = new ArrayList<OrderDetails>();

				for (int i = 0; i < list.size(); i++) {
					int cantitate = Integer.parseInt(itemMenuTable.getModel().getValueAt(i, 4).toString());
					if (cantitate > 0) {
						order.add(new OrderDetails(cantitate, list.get(i)));
					}
				}
			
				notifyObservers(order);
			}
		});

		viewOrdersBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				viewOrdersFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				viewOrdersFrame.setBounds(200, 200, 700, 450);
				viewOrdersFrame.setResizable(true);
				viewOrdersFrame.setVisible(true);
				viewOrdersFrame.setLayout(null);

				String[] arg = new String[CustomerOrder.class.getDeclaredFields().length];

				int idx = 0;
				for (Field field : CustomerOrder.class.getDeclaredFields()) {
					arg[idx++] = field.getName();
				}

				int rows = new DataMenuItem().getNumberOfItems();

				@SuppressWarnings("serial")
				DefaultTableModel model = new DefaultTableModel(arg, rows) {

					@Override
					public boolean isCellEditable(int row, int column) {
						// TODO Auto-generated method stub
						return false;
					}

					public Class<?> getColumnClass(int col) {
						return String.class;
					}
				};

				int index = 0;
				for (int j = 1; j <= rows; j++) {
					CustomerOrder customerOrder = new AbstractAccess<CustomerOrder>(CustomerOrder.class)
							.getColumnByID(j);
					if (customerOrder != null) {
						model.setValueAt(customerOrder.getId(), index, 0);
						model.setValueAt(customerOrder.getTableNo(), index, 1);
						model.setValueAt(customerOrder.getPrice(), index, 2);
						index++;
					}
				}

				itemMenuTable.setModel(model);
				itemMenuTable.setBounds(30, 50, 500, 200);
				itemMenuTable.setPreferredScrollableViewportSize(new Dimension(600, 200));

				DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
				cellRenderer.setHorizontalAlignment(JLabel.CENTER);
				itemMenuTable.setCellSelectionEnabled(false);
				itemMenuTable.setRowSelectionAllowed(true);
				itemMenuTable.setDefaultRenderer(String.class, cellRenderer);
				itemMenuTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
				itemMenuTable.getTableHeader().setReorderingAllowed(false);
				viewOrdersFrame.add(itemMenuTable);

				JScrollPane pane = new JScrollPane(itemMenuTable);
				pane.setBounds(30, 50, 610, 200);
				pane.setVisible(true);
				viewOrdersFrame.add(pane);

				placeOrderBtn.setBounds(30, 260, 80, 30);
				placeOrderBtn.setVisible(true);
				viewProductsFrame.add(placeOrderBtn);
			}
		});

		generateBillBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				new Restaurant().generateBill();
			}
		});

	}

	public static void main(String[] args) {
		new WaiterGraphicalUserInterface().initWaiterUI();
	}

	@Override
	public void registerObserver(Observer observer) {
		// TODO Auto-generated method stub
		observers.add(observer);
	}

	@Override
	public void removeObserver(Observer observer) {
		// TODO Auto-generated method stub
		observers.remove(observer);
	}

	@Override
	public void notifyObservers(List<OrderDetails> order) {
		// TODO Auto-generated method stub
		chefWindow.editFrame(order, tableNo);
	}

}
